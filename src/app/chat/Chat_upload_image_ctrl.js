// Chat Upload  Image Controller
angular
    .module('app_chat')
    .controller('ChatUploadImageCtrl', ChatUploadImageCtrl);
function ChatUploadImageCtrl($scope, Upload, $timeout, $rootScope, socket) {

    $scope.upload = function (dataUrl) {

        $scope.showImageForm = false;
        $rootScope.smolImage = dataUrl;
        if ($rootScope.smolImage) {
            socket.emit('user_image_chat', {imageUser: $rootScope.smolImage})
        }

    };
    $scope.dataImg = function (data) {
        $scope.imageInNgFile = data.$ngfBlobUrl;

    };

}