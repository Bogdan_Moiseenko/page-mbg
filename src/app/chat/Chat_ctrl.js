//Chat controller
angular
    .module('app_chat')
    .controller('ChatCtrl', ChatCtrl);
function ChatCtrl(socket, $scope, $rootScope) {
    $scope.img1 = '';
    var vm = this;
    /*  elChatText = angular.element(document.querySelector('#chatText'));*/
    $scope.wizivigStatus = false;
    $scope.wizMassage = 'Открыть редактор';
    $scope.changWizMas = function () {
        if ($scope.wizivigStatus) {
            $scope.wizMassage = 'Закрыть редактор';
        } else {
            $scope.wizMassage = 'Открыть редактор';
        }
    };


    $scope.chattext = [];


    $scope.editorOptions = {
        language: 'ru',
        uiColor: '#000000'
    };


    vm.sendMessage = function () {
        if ($scope.textInput) {
            socket.emit('new_message', {chat_message: $scope.textInput});
            $scope.textInput = '';
        }
    };
// Data processing с WYSIWYG

    vm.sendTxtWizivig = function () {
        if ($scope.textWizivig) {
            socket.emit('new_message', {chat_message: $scope.textWizivig, img: $rootScope.smolImage});
            console.log($scope.textWizivig);
            $scope.textWizivig = "";
        } else if ($scope.textInput) {
            socket.emit('new_message', {chat_message: $scope.textInput, img: $rootScope.smolImage});
            $scope.textInput = '';
        }
    };
    $rootScope.sendKey = function (keyCode) {

        if (keyCode === 10 && $scope.textWizivig) {
            socket.emit('new_message', {chat_message: $scope.textWizivig, img: $rootScope.smolImage});
            $scope.textWizivig = "";
        } else if (keyCode === 10 && $scope.textInput) {
            socket.emit('new_message', {chat_message: $scope.textInput, img: $rootScope.smolImage});
            $scope.textInput = '';
        }
    };


// now time
    function nowTime() {
        var date = new Date();
        return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }


    /*

     //Функция вывода текста на экран
     function chatText(n, d) {
     var h = '';
     var f = '<li>' + n + ' [' + nowTime() + '] ' + d + '</li>';
     $scope.chattext.push(f);

     for (var i in $scope.chattext) {
     h = $scope.chattext[i];
     }

     /!*console.log(chattext);*!/
     elChatText.prepend(h);
     }
     */


// Function save data in arr $scope.chattext chat
    function chatArrText(name, data, color, img) {
        var d = {name: name, time: ' [' + nowTime() + '] ', text: data, color: color, img: img};
        $scope.chattext.unshift(d)
    }

    socket.on('new_user_inChat', function (data) {
        /*chatText('', '<p>New User ' + '<span style="color: red">' + data.mes + '</span><p>');*/
        chatArrText('New User ', data.mes, 'red', data.img);
        console.log(data.img);
        $scope.$digest()
    });

    socket.on('new_message_fo_user', function (data) {
        /*chatText('<span style="color: #2ee512">' + '<strong> YOU  </strong>' + '</span>', data.mes);*/

        chatArrText('YOU', data.mes, '#2ee512', data.img);
        $scope.$digest()
    });

    socket.on('new_message_fo_oll', function (data) {
        /*chatText('<span style="color: #0b7197" >' + data.userName + ' ' + '</span>', data.mes);*/

        chatArrText(data.userName, data.mes, '#0b7197', data.img);
        $scope.$digest()
    });

    socket.on('user_end-seshion', function (data) {
        /*chatText("User " + '<span style="color: red">' + data.mes + '</span>' + " left the chat  ", '');*/

        chatArrText('User Lost Chat', data.mes, 'red', data.img);
        $scope.$digest()
    });


}