angular
    .module('app_registration_form')
    .controller('Registration_form_user_Ctrl', Registration_form_user_Ctrl);

function Registration_form_user_Ctrl(socket, $scope, $rootScope, $location, $localStorage) {
    var vm = this;



    vm.chaingeName = function (d) {
        socket.emit('changeName', {nN: d});
    };
    vm.chaingeNikname = function(d){
        socket.emit('changeNikName', {nkN: d});
    };
    socket.on('nameErrorMessage', function (data) {
        vm.massege = data.message;
        $scope.$apply();
    });


    vm.massageForm = function (m) {
        vm.massege = m;

    };

    vm.exitForm = function () {
        $rootScope.nameUser = '';
        $rootScope.statusEnterExit = false;
        $location.path('/menu_app').replace();
    };


    $scope.upload = function (dataUrl) {
        console.log(dataUrl);
        $scope.showImageForm = false;
        $rootScope.smolImage = dataUrl;


    };
    $scope.dataImg = function (data) {
        $scope.imageInNgFile = data.$ngfBlobUrl;

    };


    $scope.AddUser = function () {
        if ($scope.formUser.$valid && $scope.user.code1 && $scope.user.code2) {
            if ($scope.user.code1 === $scope.user.code2) {

                socket.emit('new_user', {dataUser: $scope.user}); //{name: $scope.name, code: $scope.code2, chatName: $scope.chatUserName});
                if ($rootScope.smolImage) {
                    socket.emit('user_image_chat', {imageUser: $rootScope.smolImage})
                }
            } else if ($scope.user.code1 != $scope.user.code2) {
                vm.massege = "You do not correctly Confirm password";
                $scope.user.code2 = '';
            }
        } else if (!$scope.formUser.$valid) {
            vm.massege = "Введите данные"
        }

    };

    socket.on('new_user_enter', function (data) {

        vm.massageForm(data.message);
        $rootScope.nameUser =  data.name;
        $rootScope.nameChatUser =  data.userchatName;
        $rootScope.statusEnterExit = true;

        // Add data to localstorage
        $scope.$storage = $localStorage.$default({
            nameChatUser: data.userchatName,
            statusEnterExit: true,
            nameUser:  data.name,
            registrationOn: false
        });


        $location.path('/menu/main-page');
        $scope.$apply();

    });
    socket.on('errorChatName', function (data) {
        vm.massege = data.mesError;

        $scope.user.chatUserName = '';
        $scope.$apply();
    });
    socket.on('errorName', function (data) {
        vm.massageForm(data.mesError);
        $scope.user.name = '';
        $scope.$apply();
    });


}
