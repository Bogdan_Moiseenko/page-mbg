angular.module('app-main-page')
    .controller('Main_page_ctrl',Main_page_ctrl);

function Main_page_ctrl($scope,  $rootScope){

    $(document).ready(function () {
        /* fixed - класс элемента, который фиксируем */
        var offset = $('.fixed').offset();
        var topPadding = 80; /* отступ элемента от верха страницы */
        $(window).scroll(function() {
            if ($(window).scrollTop() > offset.top) {
                $('.fixed').stop().animate({marginTop: 20 +($(window).scrollTop()/55) /*$(window).scrollTop() - offset.top - topPadding * 5*/});
            }
            else {
                $('.fixed').stop().animate({marginTop: 0});
            }
        });
    });

    $(document).ready(function () {
        var win = $(window);
        var li = $('li');
        var touch1 = $('#touch1 b');
        var touch2 = $('#touch2 b');
        var nav1 = $('.nav1');
        var nav2 = $('.nav2');

        nav1.hide();
        nav2.hide();

        $(touch1).mousemove(function (e) {
            e.preventDefault();
            nav1.show(500);
        });

        $(li).mouseup(function () {
            nav1.hide(1500);
            nav2.hide(1500);
        });
        $(win).mouseup(function(){
            nav1.hide(1500);
            nav2.hide(1500);
        })

        $(touch2).mousemove(function(e){
            nav2.show(500);
        })


    });
}
