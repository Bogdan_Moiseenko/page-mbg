angular.module('konstrust_new', [])
    .controller('KonstructorCtrl', KonstructorCtrl);

function KonstructorCtrl($scope, myAlert) {

    var vm = this;
    vm.action = function (n) {
        if (n === 1) {

            function BigAnimal() {
                this.name = "Мышь";
                return {name: "Годзилла"}; // <-- возвратим объект
            }

            $scope.nameButton = new BigAnimal().name;
            myAlert('first', new BigAnimal().name);
        }
        if (n === 2) {
            function User(name) {
                this.name = name;
                this.sayHi = function () {
                    $scope.nameButton2 = ( "Моё имя: " + this.name );
                    myAlert('', "Моё имя: " + this.name);
                };
            }

            var ivan = new User("Иван");
            ivan.sayHi();
        }
        if (n === 3) {
            function User2(firstName, lastName) {
                // вспомогательная переменная
                var phrase = "Привет";

                // вспомогательная вложенная функция
                function getFullName() {
                    return firstName + " " + lastName;
                }

                this.sayHi = function () {
                    $scope.nameButton3 = phrase + ", " + getFullName();
                    myAlert('', phrase + ", " + getFullName()); // использование
                };
            }

            var vasya = new User2("Вася", "Петров");

            vasya.sayHi(); // Привет, Вася Петров
        }
    };


}