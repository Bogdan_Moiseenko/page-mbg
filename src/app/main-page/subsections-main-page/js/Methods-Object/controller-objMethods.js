angular.module('objectMethods', [])
    .controller('ObjMethods', ObjMethods);

function ObjMethods($scope) {
    var vm = this;
    $scope.signationButton = 'ON  ' + '  ';
    $scope.obj = '';
    $scope.signature = '';

    vm.action = function (n) {
        var user = {
            name: 'Василий'
        };

        if (n === 1) {
            user = {
                // метод
                sayHi: function () {
                    alert('Привет!');
                    $scope.obj = 'Привет';
                    $scope.signature = ' user.sayHi() ';
                }
            };
            // Вызов
            user.sayHi();
        }
        if (n === 2) {
            user.sayHi = function () {
                alert(this.name);
                $scope.obj = this.name;
                $scope.signature = ' user.sayHi() '
            };
            user.sayHi();
        }
        if (n === 3) {
            $scope.obj = "Cannot read property 'name' of null";
            $scope.signature = ' user.sayHi() ';
            alert("Cannot read property 'name' of null")
        }
        if (n === 4) {
            user.sayHi = function () {
                showName(this);
            };
            function showName(namedObj) {
                alert(namedObj.name);
                $scope.obj = namedObj.name;
                $scope.signature = ' user.sayHi() '
            }

            user.sayHi();
        }
        if (n === 5) {
            user = {firstName: "Вася"};
            var admin = {firstName: "Админ"};

            function func() {
                alert(this.firstName);
            }

            user.f = func;
            admin.g = func;
            // this равен объекту перед точкой:
            user.f(); // Вася
            admin.g(); // Админv
            admin['g']();
        }
        if (n === 6) {
            function fun() {
                "use strict";
                alert(this); // выведет undefined (кроме IE9-)
                $scope.obj = 'undefined';
                $scope.signature = ' fun() '
            }
            fun();
        }

    }


}