angular.module('arr', [])
    .controller('ArrCtrl', ArrCtrl);

function ArrCtrl($scope, $rootScope, myAlert) {
    var arrCountrys = ["Ukraine", "Byelorussia", "Italy"];
    var iLernJS = ["Я", "сейчас", "изучаю", "JavaScript"];
    var vm = this;

    $scope.array1 = arrCountrys;
    vm.action = function (n) {
        if (n === 0) {
            $scope.array1 = ["Ukraine", "Byelorussia", "Italy"];
            myAlert('', $scope.array1);
        }
        if (n === 1) {
            $scope.array1[1] = "France";
            myAlert('', $scope.array1)
        }
        if (n === 2) {
            $scope.array1[3] = "Canada";
            myAlert('', $scope.array1);
        }
        if (n === 3) {
            myAlert('', $scope.array1.length);
        }
        if (n === 4) {
            $scope.array1.push("Japan");
            myAlert('', $scope.array1);
        }
        if (n === 5) {
            myAlert('', $scope.array1.pop())
        }
        if (n === 6) {
            myAlert('', $scope.array1.shift())
        }
        if (n === 7) {
            if ($scope.unShift) {
                $scope.array1.unshift($scope.unShift);
                $scope.unShift = "";
                myAlert('', $scope.array1);
            }
        }
        if (n === 8) {
            var per = $scope.array1;
            var per2 = per.join(',  ');
            myAlert('', per2);
            return $rootScope.arrG = per2;
        }
        if (n === 9 && typeof($rootScope.arrG) === "string") {

            $scope.array1 = $rootScope.arrG.split(', ');
            myAlert('', $scope.array1);
        }
        if (n === 10) {
            delete $scope.array1[0];
            myAlert('', $scope.array1)
        }
        if (n === 11) {
            $scope.array1 = ["Я", "изучаю", "JavaScript"];
            $scope.array1.splice(1, 1);
            myAlert('', $scope.array1);
        }
        if (n === 12) {
            var f = ["Я", "сейчас", "изучаю", "JavaScript"];
            f.splice(0, 3, "Мы", "изучаем");
            $scope.array1 = f;
            myAlert('',$scope.array1);
        }
        if (n === 13) {
            $scope.array1 = ["Я", "сейчас", "изучаю", "JavaScript"];
            var removed = $scope.array1.splice(0, 2);
            myAlert('',removed)
        }
        if (n === 14) {
            $scope.array1 = ["Я", "изучаю", "JavaScript"];
            $scope.array1.splice(2, 0, "сложный", "язык");
            myAlert('', $scope.array1)
        }
        if (n === 15) {
            $scope.arrayN = [1, 2, 5];
            $scope.arrayN.splice(-1, 0, 3, 4);
            myAlert('',$scope.arrayN);
            return $rootScope.arrG = $scope.arrayN
        }
        if (n === 16) {
            var arr = ["Почему", "надо", "учить", "JavaScript"];

            $scope.array1 = arr;
            myAlert('',arr.slice(1, 3)); // элементы 1, 2 (не включая 3))
        }
        if (n === 17) {
            $scope.array1 = [1, 2, 15];
            $scope.array1.sort();
            myAlert('',$scope.array1);
        }
        if (n === 18) {
            function compareNumeric(a, b) {
                if (a > b) return 1;
                if (a < b) return -1;
            }

            $scope.array1 = [1, 2, 15, 4, 8];
            $scope.array1.sort(compareNumeric);
            myAlert('',$scope.array1)
        }
        if (n === 19) {
            $scope.array1 = [1, 2, 3, 4, 5];
            $scope.array1.reverse();
            myAlert('',$scope.array1)
        }
        if (n === 20) {
            $scope.array1 = [1, 2];
            $scope.array1 = $scope.array1.concat(3, 4);
            myAlert('',$scope.array1)
        }
        if (n === 21) {
            $scope.array1 = [1, 0, false];
            myAlert('',$scope.array1.indexOf(0));
            myAlert('',$scope.array1.indexOf(false));
            myAlert('',$scope.array1.indexOf(null));
        }
        if (n === 22) {
            var user = {
                name: "Петя",
                age: 30
            };
            var k = Object.keys(user);
            myAlert('',k)
        }
        if (n === 23) {
            arr = ["Яблоко", "Апельсин", "Груша"];
            $scope.array1 = arr;
            arr.forEach(function (item, i, arr) {
                myAlert('',i + ": " + item + " (массив:" + arr + ")");
            });

        }
        if (n === 24) {
            arr = [1, -1, 2, -2, 3];

            var positiveArr = arr.filter(function (number) {
                return number > 0;
            });
            myAlert('',positiveArr); // 1,2,3
            $scope.array1 = positiveArr;
        }
        if (n === 25) {
            arr = ['HTML', 'CSS', 'JavaScript'];

            var nameLengths = arr.map(function (name) {
                return name.length;
            });

            // получили массив с длинами
            myAlert('',nameLengths); // 4,3,10
            $scope.array1 = nameLengths;
        }
        if (n === 26) {
            arr = [1, -1, 2, -2, 3];

            function isPositive(number) {
                return number > 0;
            }

            myAlert('',arr.every(isPositive)); // false, не все положительные
            myAlert('',arr.some(isPositive)); // true, есть хоть одно положительное
            $scope.array1 = '';
        }
        if (n === 27) {
            arr = [1, 2, 3, 4, 5];

// для каждого элемента массива запустить функцию,
// промежуточный результат передавать первым аргументом далее
            var result = arr.reduce(function (sum, current) {
                return sum + current;
            }, 0);

            myAlert('',result); // 15
            $scope.array1 = '';
        }

        return $rootScope.arrG = $scope.array1;
    };


    $scope.arr2 = ' [ 0, " Daniel ", { sitty: "London" } , [" Ukraine ", " Russia ", " Amerika ", " Byelorussia "], true ];';


}