angular.module('XmlHttpRec', [])
    .controller('XmlHttpCtrl', XmlHttpCtrl);

function XmlHttpCtrl($scope, myAlert) {

    var vm = this;
    $scope.btnName = 'ON';
    vm.action = function (n) {

        if (n === 0) {
            start1()
        }

        if (n === 1) {
            start2()
        }
        if (n === 2) {
            loadPhones()
        }
    };

    function start1() {
        var xhttr = new XMLHttpRequest();
        xhttr.onreadystatechange = function () {
            if (xhttr.readyState == 4 && xhttr.status == 200) {
                myAlert('xhttr.status = ' + xhttr.status, xhttr.responseText)
            }
        };

        xhttr.open('GET', '../phones.json', true);
        xhttr.send();
    }

    function start2() {
        var but = document.getElementById('md-button-id');
        var xhr = new XMLHttpRequest();

        xhr.open('GET', '../phones.json', true);

        xhr.send(); // (1)

        xhr.onreadystatechange = function () { // (3)
            if (xhr.readyState != 4) return;

            but.innerHTML = 'Готово!';

            if (xhr.status != 200) {
                myAlert(xhr.status + ': ' + xhr.statusText);
            } else {
                myAlert(xhr.responseText);
            }

        };

        but.innerHTML = 'Загружаю...'; // (2)
        /*  button.disabled = true;*/
    }


    function loadPhones() {


        var xhr = new XMLHttpRequest();

        xhr.open('GET', '../phones.json', true);

        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;

            if (xhr.status != 200) {
                // обработать ошибку
                myAlert(xhr.status + ': ' + xhr.statusText);
            } else {
                try {
                    var phones = JSON.parse(xhr.responseText);
                    myAlert(xhr.responseText);
                } catch (e) {
                    myAlert("Некорректный ответ " + e.message);
                }
                showPhones(phones);
            }

        };


    }

    function showPhones(phones) {

        phones.forEach(function (phone) {
            var li = list.appendChild(document.createElement('li'));
            li.innerHTML = phone.name;
        });

    }

}