angular.module('g-maps')
    .controller('GmapsCtrl', GmapsCtrl);

function GmapsCtrl($scope, $http) {
    (function () {
        $scope.cities = [];
        //$scope.map;
        $scope.infoBoxx = new google.maps.InfoWindow();
        var mapContainer = document.getElementById('map');
        $scope.marker = new google.maps.Marker({

            animation: google.maps.Animation.BOUNCE
        });
        mapContainer.style.width = '100%';
        mapContainer.style.height = '500px';

        $http.get('data.json').success(function (data) {
            $scope.cities = data;
        });

        $scope.initialize = function () {
            var mapOptions = {
                center: new google.maps.LatLng(50.5, 30.5),
                zoom: 8,
                /*disableDefaultUI:true, //ПРи включение этого свойства с карты уберутся все элементы управления*/

                /*  // это включит все элементы управления картой
                 panControl:true,
                 zoomControl:true,
                 mapTypeControl:true,
                 scaleControl:true,
                 streetViewControl:true,
                 overviewMapControl:true,
                 rotateControl:true,
                 //-------------------------  */
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $scope.map = new google.maps.Map(mapContainer, mapOptions);
        };

        $scope.showCity = function (city) {
            var coords = new google.maps.LatLng(city.lat, city.lng);
            $scope.infoBoxx.setContent(city.city + ' - ' + city.desc);
            $scope.infoBoxx.setPosition(coords);
            $scope.infoBoxx.open($scope.map);
            $scope.map.setCenter(coords);

            /*Добавляю анимированый маркер*/
            $scope.marker.setPosition(coords);
            $scope.marker.setMap($scope.map);


            google.maps.event.addListener($scope.marker, 'click', function () {
                $scope.map.setZoom(15);
                $scope.map.setCenter($scope.marker.getPosition());
            });
            /*Добавляю окрудность вокруг выбраной точки*/
            var myCity = new google.maps.Circle({
                center: coords,
                radius: 20000,
                strokeColor: "#aa4f59",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#43FFB5",
                fillOpacity: 0.4
            });

            myCity.setMap($scope.map);


            /*Добавляет информацию прищелчке по маркеру*/
            var infowindow = new google.maps.InfoWindow({
                content: "Привет Вы нажали на прыгающий МАРКЕР"
            });

            google.maps.event.addListener($scope.marker, 'click', function () {
                infowindow.open($scope.map, $scope.marker);
            });


            /*Добавляет маркер с сообщением долготы и широты*/
            google.maps.event.addListener($scope.map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: $scope.map
                });
                /*Создаём новый маркер при наведение на него появляется подсказка с длинной и широтой*/
                google.maps.event.addListener(marker, 'mouseover', function () {
                    var infowindow = new google.maps.InfoWindow({
                        content: 'Latitude: ' + location.lat() +
                        '<br>Longitude: ' + location.lng()
                    });
                    infowindow.open($scope.map, marker);
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infowindow = null;
                    })
                });


            }

        }
    })();


    (function () {
        var map1div = document.getElementById('map1');
        /* map1div.style.width = '100%';
         map1div.style.height = '500px';*/
        var map1options = {
            center: new google.maps.LatLng(49.995822, 36.24183654),
            zoom: 8,
            disableDefaultUI: true, //ПРи включение этого свойства с карты уберутся все элементы управления
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map1 = new google.maps.Map(map1div, map1options);
    })();


    /* id map2 Добавить маркер*/
    (function () {

        var mapDiv = document.getElementById('map2');
        var centrMap = new google.maps.LatLng(49.995822, 36.24183654);

        var marker = new google.maps.Marker({
            position: centrMap
        });
        var mapOptions = {
            center: centrMap,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var mapG = new google.maps.Map(mapDiv, mapOptions);
        marker.setMap(mapG);
        $scope.onOff = 'ON';
        $scope.animMark = function (n) {
            if (n === 0) {
                if (marker.animation) {
                    marker.setMap(null);
                    marker = new google.maps.Marker({
                        position: centrMap
                    });
                    marker.setMap(mapG);
                    $scope.onOff = 'ON'
                } else {
                    marker.setMap(null);
                    marker = new google.maps.Marker({
                        position: centrMap,
                        animation: google.maps.Animation.BOUNCE
                    });
                    marker.setMap(mapG);
                    $scope.onOff = 'OFF'
                }
            }
            if (n === 1) {
                marker.setMap(null);
                marker = new google.maps.Marker({
                    position: centrMap,
                    icon: 'images/Standing Man-50.png'
                });
                marker.setMap(mapG);
            }
            if (n === 2) {
                var solonitchevka = new google.maps.LatLng(49.9922, 36.04957580566406);
                var merepha = new google.maps.LatLng(49.80076850316848, 36.0516357421875);
                var ogulchi = new google.maps.LatLng(49.895519055187826, 35.814056396484375);
                var myTrip = [solonitchevka, merepha, ogulchi, solonitchevka];
                var flightPath = new google.maps.Polygon({
                    path: myTrip,
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 1,
                    fillColor: "#43FFB5",
                    fillOpacity: 0.4
                });

                flightPath.setMap(mapG);
            }
            if (n === 3) {
                var infowindow = new google.maps.InfoWindow({
                    content: "Это надпись над маркером!"
                });

                infowindow.open(mapG, marker);

            }
        };
    })();

    (function () {
        var markers = [];
        var map1div = document.getElementById('map3');
        /*Не забудь прописать в стилях размер крты #map3{width: .... ; heigth: ......;}*/
        var centrMap = new google.maps.LatLng(49.975822, 36.24183654);
        var map1options = {
            center: new google.maps.LatLng(49.995822, 36.24183654),
            zoom: 8,
            disableDefaultUI: true, //ПРи включение этого свойства с карты уберутся все элементы управления
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map3 = new google.maps.Map(map1div, map1options);

        /* var*/
        marker = new google.maps.Marker({
            position: centrMap,
            title: 'Click to zoom'
        });

        marker.setMap(map3);

        /*При нажатии на маркер карта увеличивается*/
        google.maps.event.addListener(marker, 'click', function () {
            map3.setZoom(15);
            map3.setCenter(marker.getPosition());
        });

        /*Возврат карты обратно на маркер в центре*/
        google.maps.event.addListener(map3, 'center_changed', function () {
            window.setTimeout(function () {
                map3.panTo(marker.getPosition());
            }, 7000);
        });


        /*Этот код добавляет сообщение к маркеру при нажатие*/
        var infowindow = new google.maps.InfoWindow({
            content: "Сообщение появилось потому что Вы нажали на маркер!"
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map3, marker);
        });


        /*Код при клике рисует маркер*/
        google.maps.event.addListener(map3, 'click', function (event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map3
            });
            /*Этот код выведит сообщение при добавление маркера*/
            /*var infowindow = new google.maps.InfoWindow({
             content: 'Latitude: ' + location.lat() +
             '<br>Longitude: ' + location.lng()
             });
             infowindow.open(map3, marker);*/


            /* при наведение на маркер появляется подсказка с длинной и широтой*/
            google.maps.event.addListener(marker, 'mouseover', function () {
                var infowindow = new google.maps.InfoWindow({
                    content: 'Latitude: ' + location.lat() +
                    '<br>Longitude: ' + location.lng()
                });
                infowindow.open(map3, marker);
                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow = null;
                })
            });


            /*Этот блок кода отвечает за удаление маркеров*/
            markers.push(marker);
            $scope.clearMarkers = function () {

                if (markers) {
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setMap(null);
                    }
                }

            }

        }


    })()


}






















