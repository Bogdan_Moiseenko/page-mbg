angular.module('ui.bootstrap.demo').controller('CarouselDemoCtrl', function ($scope, $rootScope, socket) {
    $scope.myInterval = 2000;
    $scope.noWrapSlides = false;
    $scope.chekUpload = false;

    var slides = $rootScope.slides;

    //Function add an image
    $scope.addImage = function (dataUrl, hk) {
        console.log($rootScope.statusEnterExit , !$rootScope.registrationOn);
        if ($rootScope.statusEnterExit && !$rootScope.registrationOn) {


            if (dataUrl && $scope.chekUpload) {

                slides.push({
                    image: dataUrl,
                    text: $scope.imgText
                });
                $scope.uploadMessage = false;

                socket.emit('user_image_karusel', {imageUser: slides});
            } else {
                $scope.uploadMessage = "Please Select  Image";
            }
            $scope.imgText = "";

        } else {
            $scope.uploadMessage = "Please login";

        }


    };

    //Function remove image
    $scope.deleteImage = function (d) {
        for (var i in slides) {

            if (d == slides[i].$$hashKey) {
                slides.splice(i, 1);
                socket.emit('user_image_karusel', {imageUser: slides});
            }
        }
    };

    //Pressing the Enter button will start the function add an image
    $scope.addAnImage = function (keyCode, d) {
        console.log(keyCode);
        if (keyCode == 13) {

            $scope.addImage(d)
        }
    };


});


