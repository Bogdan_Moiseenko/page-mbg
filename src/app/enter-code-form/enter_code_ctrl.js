angular.module('app_enter_code')
    .controller('EntercodeCtrl', EntercodeCtrl);
function EntercodeCtrl($scope, socket, $rootScope, $location, $localStorage) {
    var vm = this;

    vm.showFormRegistration = true;

    vm.messageUserErrorName = '';
//The function of sending the user data to the server
    vm.enterNameCode = function () {
        if (vm.name && vm.code) {
            socket.emit('data_user', {name: vm.name, code: vm.code});
            vm.messageUserErrorName = '';

        }
        else {
            vm.messageUserErrorCode = 'Wrong data';
            console.log(vm.messageUserErrorCode)
        }
    };


 //The answer from the server that the user exists in the function performed collbac
// adding his name to the discovery of the hidden menu items or add images
    socket.on('user_on', function (data) {
        $rootScope.smolImage = data.userImage;
        if (data.userImageAlbom) {
            $rootScope.slides = [];
            var imageAlbom = data.userImageAlbom;
            for (var i in imageAlbom) {
                $rootScope.slides.push({
                    image: imageAlbom[i].image
                });
            }

        }
        $scope.$storage = $localStorage.$default({
            nameChatUser: data.userchatName,
            statusEnterExit: true,
            nameUser:data.name,
            registrationOn: false
        });
        $rootScope.nameChatUser =  data.userchatName;
        $rootScope.statusEnterExit = true;
        $rootScope.nameUser =  data.name;
        $rootScope.registrationOn = false;
        $location.path('/menu/main-page');
        $scope.$apply();
    });

//The answer from the server if the code is not correct
    socket.on('user_error_code', function (data) {
        vm.messageUserErrorCode = data.messege;
        vm.messageUserErrorName = "";
        vm.code = '';
        $scope.$apply();
    });

//response from the server if the wrong name
    socket.on('user_error_name', function (data) {
        vm.messageUserErrorName = data.messege;
        vm.messageUserErrorCode = "";
            $scope.$apply();
    });

}

