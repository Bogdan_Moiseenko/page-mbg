angular.module('menu')
    .controller('MenuCtrl', MenuCtrl);


function MenuCtrl($scope, socket, $rootScope, $localStorage) {
    var vm = this;


    $(document).ready(function () {
        var win = $(window);
        var li = $('li');
        var touch = $('#touch-menu  b');
        var menu = $('.nav');

        $(touch).mousemove(function (e) {
            e.preventDefault();
            menu.show(500);
        });

        $(li).mouseup(function () {
            var wid = $(window).width();
            if (wid < 768) {
                menu.hide(500);
            }
        });
        $(win).mouseup(function () {
            var wid = $(window).width();
            if (wid < 768) {
                menu.hide(500);
            }
        });


        $(window).resize(function () {
            var wid = $(window).width();
            if (wid > 769 && menu.is(':hidden')) {
                menu.removeAttr('style');

            }
        });
    });

    (function () {
        var h_hght = 120; // высота шапки
        var h_mrg = 0;    // отступ когда шапка уже не видна
        $(function () {
            $(window).scroll(function () {
                var top = $(this).scrollTop();
                var elem = $('.containerr');
                if (top + h_mrg < h_hght) {
                    elem.css('top', (h_hght - top));
                } else {
                    elem.css('top', h_mrg);
                }
            });
        });
    })();


    $scope.$storage = $localStorage;
    if ($scope.$storage) {

        socket.emit('comparison_data', {name: $scope.$storage.nameUser, userchatName: $scope.$storage.nameChatUser});

        socket.on('comparison_data_answer', function (data) {

            if (data.userImageAlbom) {
                $rootScope.slides = [];
                var imageAlbom = data.userImageAlbom;
                for (var i in imageAlbom) {
                    $rootScope.slides.push({
                        image: imageAlbom[i].image
                    });
                }

            }


            $rootScope.nameChatUser = $scope.$storage.nameChatUser;
            $rootScope.statusEnterExit = $scope.$storage.statusEnterExit;
            $rootScope.nameUser = $scope.$storage.nameUser;
            $rootScope.registrationOn = false;
            $rootScope.smolImage = data.userImage;

            $scope.$apply();

        })
    }


    vm.menuExit = function () {
        socket.emit('client_user_lost_sistem', {message: $rootScope.nameChatUser});
        $rootScope.nameChatUser = '';
        $rootScope.statusEnterExit = false;
        $rootScope.nameUser = '';
        $rootScope.statusUser = false;
        $rootScope.smolImage = '';
        $rootScope.slides = [];

        delete $scope.$storage.nameChatUser;
        delete $scope.$storage.statusEnterExit;
        delete $scope.$storage.nameUser;
        delete $scope.$storage.registrationOn;

    };


}