'use strict';

angular.module('App', ['ui.router',
    'ngMaterial',
    'ui.bootstrap',
    'ngAnimate',
    'ngMessages',
    'ngStorage',
    'menu',
    'calkApp',
    'ui.bootstrap.demo',
    'app_enter_code',
    'app_registration_form',
    'app_description',
    'app_chat',
    'g-maps',
    'app-main-page',
    'arr',
    'konstrust_new',
    'ajax1',
    'XmlHttpRec',
    'objectMethods'])
    .config(function ($stateProvider, $urlRouterProvider) {

        // Now set up the states
        $stateProvider
            .state('menu', {
                url: "/menu",
                templateUrl: "menu/menu.html",
                controller: "MenuCtrl",
                controllerAs: "MenuCtrlVM"
            })
            .state('menu.main-page', {
                url: '/main-page',
                templateUrl: 'main-page/main-page.html',
                controller: 'Main_page_ctrl'
            })
            .state('menu.enter_code_form', {
                url: '/enter_code_form',
                views: {
                    enter_code_form: {
                        templateUrl: 'enter-code-form/enter_code.html',
                        controller: 'EntercodeCtrl',
                        controllerAs: 'enCodVM'
                    }
                }
            })
            .state('menu.registration_user_form', {
                url: '/registration_user_form',
                views: {
                    registration_user_form: {
                        templateUrl: 'registration-user-form/registration_user.html',
                        controller: 'Registration_form_user_Ctrl',
                        controllerAs: 'registVM'
                    }
                }
            })
            .state('menu.chat_app', {
                url: '/chat_app',
                views: {
                    chat: {
                        templateUrl: 'chat/chat.html',
                        controller: 'ChatCtrl',
                        controllerAs: 'chatVM'
                    }
                }
            })
            .state('menu.albom', {
                url: '/albom',
                templateUrl: 'albom/foto_albom.html',
                controller: 'CarouselDemoCtrl',
                controllerAs: 'AlbomVM'
            })
            .state('menu.calc', {
                url: '/calc',
                templateUrl: 'calk/calk.html',
                controller: 'newCtrl',
                controllerAs: 'CtrrlVM'

            })
            .state('menu.description', {
                url: '/description',
                templateUrl: 'description_of_the_page/description.html',
                controller: 'DescriptionCtrl'
            })
            .state('menu.google-maps', {
                url: '/google-maps',
                templateUrl: 'google-maps/maps-index.html',
                controller: 'GmapsCtrl',
                controllerAs: 'GmapsVM'
            })
            // подразделы main-page
            .state('menu.main-page.html', {
                url: '/html',
                templateUrl: 'main-page/subsections-main-page/html.html'
            })
            .state('menu.main-page.css', {
                url: '/css',
                templateUrl: 'main-page/subsections-main-page/css.html'
            })
            .state('menu.main-page.js', {
                url: '/js',
                templateUrl: 'main-page/subsections-main-page/js/javaScript.html'
            })
            .state('menu.main-page.angularJs', {
                url: '/angularJs',
                templateUrl: 'main-page/subsections-main-page/angularJS.html'
            })
            .state('menu.main-page.array', {
                url: '/array',
                templateUrl: 'main-page/subsections-main-page/js/array/arr.html',
                controller: 'ArrCtrl',
                controllerAs: 'ArrVM'
            })
            .state('menu.main-page.objMethods', {
                url: '/objMethods',
                templateUrl: 'main-page/subsections-main-page/js/Methods-Object/objMethods.html',
                controller: 'ObjMethods',
                controllerAs: 'ObjMet'
            })
            .state('menu.main-page.constructor', {
                url: '/constructor',
                templateUrl: 'main-page/subsections-main-page/js/Constructor-new/constructor.html',
                controller: 'KonstructorCtrl',
                controllerAs: 'KNvm'
            })
            .state('menu.main-page.ajax', {
                url: '/ajax',
                templateUrl: 'main-page/subsections-main-page/ajaxAndComet/ajax1.html',
                controller: 'Ajax1Ctrl',
                controllerAs: 'AjaxVM'
            })
            .state('menu.main-page.xmlHttp', {
                url: '/xmlHttp',
                templateUrl: 'main-page/subsections-main-page/ajaxAndComet/XMLHttpRequest/XMLHttp.html',
                controller: 'XmlHttpCtrl',
                controllerAs: 'XmlHttpCtrlVM'
            })
            .state('menu.main-page.XMLHttpRequest-POST', {
                url: '/XMLHttpRequest-POST',
                templateUrl: 'main-page/subsections-main-page/ajaxAndComet/XMLHttpRequestPOST/post.html'
            })
            .state('menu.main-page.XMLHttpRequest-cross-domain', {
                url: '/XMLHttpRequest-cross-domain',
                templateUrl: 'main-page/subsections-main-page/ajaxAndComet/XMLHttpRequest-cross-domain/cross-domain.html'
            })
            .state('menu.main-page.XMLHttpRequest-progress-indicator', {
                url: '/XMLHttpRequest-progress-indicator',
                templateUrl: 'main-page/subsections-main-page/ajaxAndComet/XMLHttpRequest-progress-indicator/progress indicator.html'
            });

        /* $urlRouterProvider.otherwise('/menu');*/

    })
    .factory('socket', function () {
        var socket = io.connect('http://localhost:3070');
        return socket;


    })

    // Сервис который создаёт alert
    .factory('myAlert', function () {

        function createMessage(title, body) {
            var container = document.createElement('div');

            container.innerHTML = '<div class=" my-message "> \
        <div class="my-message-title">' + title + '</div>\
        <div class=" my-message-body">' + body + '</div> \
        <input class="my-message-ok  " type="button" value="OK">\
        </div>';

            return container.firstChild
        }

        function positionMessage(elem) {
            elem.style.position = 'absolute';

            var scroll = document.documentElement.scrollTop || document.body.scrollTop;
            var scroll2 = document.documentElement.clientHeight;

            elem.style.top = scroll + scroll2 / 3 + 'px';

            elem.style.left = document.documentElement.clientWidth / 3 +'px' /* Math.floor(document.body.clientWidth / 2) - 180 + 'px'*/;
        }

        function addCloseOnClick(messageElem) {

            var input = messageElem.getElementsByTagName('INPUT')[0];

            input.onclick = function () {
                messageElem.parentNode.removeChild(messageElem)
            }

        }

        return function myAlert(title, body) {

            if(body == undefined) body = '';
            if(title == undefined) title ='';

            // создать
            var messageElem = createMessage(title, body);

            // позиционировать
            positionMessage(messageElem);

            // добавить обработчик на закрытие
            addCloseOnClick(messageElem);

            // вставить в документ
            document.body.appendChild(messageElem)
        }


    })

    .run(function ($rootScope) {
        $rootScope.statusEnterExit = false;
        $rootScope.nameUser = '';
        $rootScope.nameChatUser = '';
        $rootScope.statusUser = true;
        $rootScope.registrationOn = '';

        $rootScope.smolImage = '';
        $rootScope.slides = [];

    });
