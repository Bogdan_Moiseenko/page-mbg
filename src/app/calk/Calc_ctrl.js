angular.module('calkApp')
    .controller('newCtrl', function ($scope, $rootScope) {
        var vm = this;
        vm.result = 0;
        var num = '';
        $scope.numberDate = function (n) {
            num = num + n;
            vm.result = num;
        };
        $scope.sum = function (h) {
            try {
                num = eval(num);
                vm.result = num.toLocaleString();
            } catch (e) {
                vm.result = 'Error:';
            }
        };
        $scope.sqr = function () {
            num = Math.sqrt(num);
            vm.result = num;
        };
        $scope.power = function () {
            num = Math.pow(num, 2);
            vm.result = num;
        };
        $scope.delOne = function () {
            vm.result += '';
            num = vm.result.substring(0, num.length - 1);
            if(num) {
                vm.result = num
            }else{
                vm.result = '0';
            }
        };
        $scope.del = function () {
            num = '';
            vm.result = '0 ';
        };


        $rootScope.pressKey = function (keyCode) {
            console.log(keyCode);
            if (keyCode >= 48 && keyCode <= 57) {
                $scope.numberDate(keyCode - 48);
            } else if (keyCode == 45) {
                $scope.numberDate('-');
            } else if (keyCode == 43) {
                $scope.numberDate('+');
            } else if (keyCode == 42) {
                $scope.numberDate('*');
            } else if (keyCode == 47) {
                $scope.numberDate('/');
            } else if (keyCode == 13) {
                $scope.sum('=');
            } else if (keyCode == 127) {
                $scope.del()
            } else if (keyCode == 32) {
                $scope.delOne()
            } else if (keyCode == 46) {
                $scope.numberDate('.')
            }else if (keyCode == 61) {
                $scope.sum('=');
            }
        };
    });

