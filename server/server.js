var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    bodyParser = require("body-parser"),
    port = 3070;

/*/!*Redis-----------*!/
var redis = require('redis');
var client = redis.createClient(); //creates a new client
client.on('connect', function () {
    console.log('connected');
});*/
/*client.hmset("hosts", "mjr", "1", "another", "23", "home", "1234");
 client.hmset("hosts", "mjr34", "5451", "an345345other", "34543523", "ho345435me", "3453451234");
 client.hgetall("hosts", function (err, obj) {
 console.dir(obj);
 });*/
/*----------------*/


app.use(express.static(__dirname + '../../src/app'));
app.use(bodyParser.urlencoded({extended: false}));

console.log('server on port' + port);
console.log('Страница доступна по адресу http://localhost:' + port + '/#/menu/main-page/array');


/*XMLHttpRequest  Ajax*/

app.get('/', function (req, res) {
    res.sendfile("index.html");
});
app.post('/login', function (req, res) {
    console.log(req.body);
    var user_name = req.body.name;
    var password = req.body.pass;
    console.log("From Client pOST request: User name = " + user_name + " and password is " + password);
    var answer = {name: user_name, pass: password};
    res.end('Name = ' + user_name + ' ' + ' AND password = ' + password);
});

//-----enter code, registration forms
// ---- varibals form
var usersDats = {Jon: '12345'},
    chatUsersName = {JonOnChat: 'Jon'},
    usersImageChat = {},
    userAlbom = {};


// Проверка имени
function nameControl(newName) {
    for (var name in usersDats) {
        if (name === newName) {
            return false;
        }
    }
    return true;
}

//Проверка имени на чате
function controlChatName(chNam) {
    for (var i in chatUsersName) {
        if (i === chNam) {
            return false;
        }
    }
    return true
}


io.on('connection', function (ser) {
    ser.usernameChat = '';
    var userOn = false;

    function addPhotoToChat() {
        if (ser.usernameChat) {
            for (var i in usersImageChat) {
                if (i === ser.usernameChat) {
                    return usersImageChat[i];

                }
            }
        }
    }

    function addFotoToAlbom() {
        if (ser.usernameChat) {
            for (var i in userAlbom) {
                if (i === ser.usernameChat) {
                    return userAlbom[i];
                }
            }
        }
    }

    //checkUser in base
    ser.on('changeName', function (data) {
        // console.log( data.nN)
        if (!nameControl(data.nN)) {
            ser.emit('nameErrorMessage', {message: 'A user ' + data.nN + ' already exists'})
        } else {
            ser.emit('nameErrorMessage', {message: false})
        }
    });
    ser.on('changeNikName', function (data) {
        if (!controlChatName(data.nkN)) {
            ser.emit('nameErrorMessage', {message: 'Такой НИКНЕЙМ уже зарегистрирован'})
        } else {
            ser.emit('nameErrorMessage', {message: false})
        }
    });
    //Registration Form
    ser.on('new_user', function (data) {

        var nameUser = data.dataUser.name,
            chatName = data.dataUser.chatUserName;


        if (nameControl(nameUser) && controlChatName(chatName)) {

            /*redis db*/
            /* client.hmset(nameUser,chatName,data.dataUser.code2);
             client.hgetall(nameUser, function (err, obj) {
             console.log(obj);
             });*/


            usersDats[nameUser] = data.dataUser.code2;
            ser.emit('new_user_enter', {name: nameUser, userchatName: chatName});
            //Chat registration
            chatUsersName[chatName] = nameUser;
            ser.usernameChat = chatName;
            userOn = true;
            addPhotoToChat();
            ser.broadcast.emit('new_user_inChat', {mes: 'New User in chat  ' + chatName, img: addPhotoToChat()});


        } else if (!controlChatName(chatName)) {
            ser.emit('errorChatName', {mesError: 'This nickname is already registered on the chat'})
        } else {
            ser.emit('errorName', {mesError: 'A user ' + nameUser + ' already exists'});

        }

    });

    //Enter Form
    ser.on('data_user', function (data) {
        ser.usernameChat = '';
        var nameUser = data.name;
        if (!nameControl(nameUser) && usersDats[nameUser] === data.code) {


            var nameUserInChat = '';
            for (var i  in chatUsersName) {
                if (chatUsersName[i] === nameUser) {
                    nameUserInChat = i;
                    /*console.log(i);*/
                }
            }

            if (ser.usernameChat != nameUserInChat) {
                userOn = true;
                ser.usernameChat = nameUserInChat;


                addPhotoToChat();
                ser.emit('user_on', {
                    messege: 'You are logged ',
                    name: nameUser,
                    userchatName: nameUserInChat,
                    userImage: addPhotoToChat(),
                    userImageAlbom: addFotoToAlbom()

                });

                ser.broadcast.emit('new_user_inChat', {mes: nameUserInChat, img: addPhotoToChat()});
            } else {
                ser.disconnect();
            }

        } else if (!nameControl(nameUser)) {
            ser.emit('user_error_code', {messege: 'Wrong password '});
        } else {
            ser.emit('user_error_name', {messege: 'Such a user is not logged '});
        }
    });
    //Comparison data
    ser.on('comparison_data', function (data) {
        var nameUser = data.name;

        for (var i  in chatUsersName) {
            if (chatUsersName[i] === nameUser) {
                ser.usernameChat = data.userchatName
                userOn = true;
                ser.emit('comparison_data_answer', {

                    name: nameUser,
                    userchatName: chatUsersName[i],
                    userImage: addPhotoToChat(),
                    userImageAlbom: addFotoToAlbom()

                });


            }
        }


    });

    //Chat

    ser.on('new_message', function (data) {
        ser.emit('new_message_fo_user', {mes: data.chat_message, img: addPhotoToChat()});
        ser.broadcast.emit('new_message_fo_oll', {
            mes: data.chat_message,
            userName: ser.usernameChat,
            img: addPhotoToChat()
        });
        console.log(data.chat_message)
    });
    //Image from chat
    ser.on('user_image_chat', function (data) {
        usersImageChat[ser.usernameChat] = data.imageUser;

        // console.log(usersImageChat)
    });

    /* ser.on('disconnect', function () {
     if (userOn) {
     ser.broadcast.emit('user_end-seshion', {mes: ser.usernameChat});


     delete ser.usernameChat;

     userOn = false;
     }

     });*/
//End sesion
    ser.on('client_user_lost_sistem', function (data) {
        ser.broadcast.emit('user_end-seshion', {mes: ser.usernameChat, img: addPhotoToChat()});
        if (userOn) {
            delete ser.usernameChat;
            userOn = false;
        }
    });

// Image from albom
    ser.on('user_image_karusel', function (data) {
        userAlbom[ser.usernameChat] = data.imageUser;

    })

});
server.listen(port);










